import { objectType } from 'nexus'

export const Role = objectType({
  name: "Role",
  definition(t) {
    t.model.id()
    t.model.roleName()
    t.model.UserRole()
  },
})
