import { arg, booleanArg, intArg, list, nonNull, queryType, stringArg } from 'nexus'
import moment = require('moment-timezone')
import { resolve } from 'path/win32'
import { Attendance } from './Attendance'
import { now } from 'lodash'
import { User } from './User'
export const Query = queryType({
  definition(t) {
    t.crud.user()
    t.crud.eventItems({filtering:true})
    t.crud.users({ filtering: true, ordering: true, pagination: true })
    t.crud.attendance()
    t.crud.role()
    t.crud.userRole()
    t.nullable.field('login', {
      type: 'User',
      args: {
        email: nonNull(stringArg()),
        password: nonNull(stringArg())
      },
      resolve: async (parent, args, ctx) => {
        var curuser = await ctx.prisma.user.findFirst({
          where: { email: args.email},
        });
        if(curuser!=null){
        if(curuser.password==args.password){
            return curuser
        }
        else{
          return "Wrong password";
        }
      }
      return "no user found";
      },
    },),
    t.nullable.field('getEvents', {
      type: 'EventItem',
      resolve: (parent, args, ctx) => {
        return ctx.prisma.eventItem.findFirst();
      }
    }),
    t.nonNull.field('reqUsersAll', {
      type: list('Attendance'),
      resolve: (parent, args, ctx) => {
        return ctx.prisma.attendance.findMany({
          where: {
            state: 'Request',
          },
          include: {
            User: true,
          }
        });
      }
    }),
    t.nonNull.field('reqUsersSingle', {
      type: list('Attendance'),
      args: {
        userId: nonNull(intArg())
      },
      resolve: (parent, args, ctx) => {
        return ctx.prisma.attendance.findMany({
          where: {
            state: 'Request',
            userId: args.userId
          },
          include: {
            User: true,
          }
        });
      }
    })
  }
})
