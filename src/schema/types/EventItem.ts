import { objectType } from 'nexus'

export const eventItem = objectType({
  name: "EventItem",
  definition(t) {
    t.model.id()
    t.model.eventName()
    t.model.eventTime()
    t.model.Attendance()
  },
})
