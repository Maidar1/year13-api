import { objectType } from 'nexus'

export const UserRole = objectType({
  name: "UserRole",
  definition(t) {
    t.model.id()
    t.model.userId()
    t.model.roleId()
    t.model.Role()
    t.model.User()
  },
})
