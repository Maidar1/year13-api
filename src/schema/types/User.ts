import { objectType } from 'nexus'

export const User = objectType({
  name: "User",
  definition(t) {
    t.model.id()
    t.model.firstName()
    t.model.lastName()
    t.model.email()
    t.model.password()
    t.model.Attendance()
    t.model.UserRole()
    t.model.role()
  },
})

  