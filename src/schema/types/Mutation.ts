import { intArg, mutationType, nonNull, stringArg } from 'nexus'
import moment = require('moment-timezone')
import { prisma } from '@prisma/client'
import { find } from 'lodash'
import { UniqueArgumentNamesRule } from 'graphql'

export const Mutation = mutationType({
  definition(t) {
    // t.crud.deleteOneUser()
    t.crud.updateOneUser()
    t.crud.createOneUser()
    t.crud.createOneEventItem()
    //t.crud.createOneAttendance()
    // t.crud.createOneContent()
    // t.crud.createOneContentAnswer()
    t.nonNull.field('createAttendance', {
      type: 'Attendance',
      args: {
        userId: nonNull(intArg()),
        state: nonNull(stringArg()),
        eventItemId: nonNull(intArg()),
      },
      resolve: async (parent, args, ctx) => {
        var beforeAttend = await ctx.prisma.attendance.findFirst({
          where: {
            eventItemId: args.eventItemId,
            userId: args.userId,
          },
          orderBy: [
            {
              aDay: 'desc',
            },
          ],
        })
        if (beforeAttend != null) {
          var date1 = new Date(beforeAttend.aDay)
          var date2 = new Date()
          if (
            date1.getFullYear() == date2.getFullYear() && date1.getMonth() == date2.getMonth() && date2.getDay() == date1.getDay())
            return {}
          else {
            return ctx.prisma.attendance.create({
              data: {
                aDay: new Date(),
                state: args.state,
                userId: args.userId,
                eventItemId: args.eventItemId,
              },
            })
          }
        } else {
          return ctx.prisma.attendance.create({
            data: {
              aDay: new Date(),
              state: args.state,
              userId: args.userId,
              eventItemId: args.eventItemId,
            },
          })
        }
      },
    }), 
    t.nonNull.field('approveAttendance', {
      type: 'Attendance', 
      args: {
        attendanceId: nonNull(intArg())
      },
      resolve: (parent, args, ctx) => {
          return ctx.prisma.attendance.update({
            where: {
             id: args.attendanceId
            },
            data: {
              state: "Attendance"
            }
          });  
      }
    })
  }
})
