import { objectType } from 'nexus'

export const Attendance = objectType({
  name: "Attendance",
  definition(t) {
    t.model.id()
    t.model.aDay()
    t.model.state()
    t.model.userId()
    t.model.eventItemId()
    t.model.EventItem()
    t.model.User()
  },
})
