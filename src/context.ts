import { PrismaClient, User } from '@prisma/client'
import { ExpressContext } from 'apollo-server-express/dist/ApolloServer'
import { Request, Response } from 'express'

if (process.env.DATABASE_URL === null) {
  throw Error('Provide DATABASE_URL!')
}

const prisma = new PrismaClient({
  datasources: {
    db: {
      url: process.env.DATABASE_URL!,
    },
  },
  // log: ['query'],
})

// prisma.$on('query', (e) => {
//   // console.log('Query: ' + e.query)
//   console.log('Duration: ' + e.duration + 'ms')
// })

// const pubsub = new PubSub()

export interface Context {
  prisma: PrismaClient
  req: Request
  res: Response
 }

export async function createContext(ctx: ExpressContext): Promise<Context> {
  // const auth = await verifyAuth(ctx.req)
  // const sub = auth.sub
  // const loggedUserId = auth && (await memoizedGetUserIdBySub(sub))

  return {
    ...ctx,
    prisma,

  }
}
