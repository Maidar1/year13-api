import { config } from 'dotenv-flow'
config()
import { ApolloServer } from 'apollo-server-express'
import cookieParser from 'cookie-parser'
import express from 'express'
import { applyMiddleware } from 'graphql-middleware'
import { graphqlUploadExpress } from 'graphql-upload'
import * as HTTP from 'http'
import { createContext } from './context'
import { schema } from './schema'

const port = process.env.PORT || 5000
const host = process.env.HOST || 'localhost'

const apollo = new ApolloServer({
  schema: applyMiddleware(schema),
  context: createContext,
 })

const app = express()
  // .use(cookieParser(process.env.SESSION_SECRET || 's4per$ecret'))
  .use(
    '/graphql',
    graphqlUploadExpress({ maxFileSize: 20 * 1000000, maxFiles: 10 }),
  )

const http = HTTP.createServer(app)

apollo.applyMiddleware({ app })
// apollo.installSubscriptionHandlers(http)

// require('./api/')(app)

http.listen(Number(port), () => {
  console.log(
    `Year13 backend service ready at => http://localhost:${port}/graphql`,
  )
})
