// const express = require('express');
// const { graphqlHTTP } = require('express-graphql');
// const { buildSchema } = require('graphql');
// import { PrismaClient } from '@prisma/client';

// const app = express();

// const fakeDatabase = {
//     1: {
//       id: 'a',
//       name: 'alice',
//     },
//     2: {
//       id: 'b',
//       name: 'bob',
//     },
//   };

// const schema = buildSchema(`

//   type Query {
//       hello: String
//   }
// `);

// const root = {
//     hello: () => {
//       return 'Hello world!';
//     },
//     manaljav: ({name, age}) => {
//         return 'nas:' + age + 'ner:' + name;
//     },
//     user: ({name}) => {
//         return fakeDatabase[name];
//     }
//   };

// app.route('/').get((req, res) => res.send('Server running'));

// app.use('/graphql', graphqlHTTP({
//     schema: schema,
//     rootValue: root,
//     graphiql: true,
//   }));

// const port = 3000;

// app.listen(3000, () => console.log('Server started on port:' + port));
